extends KinematicBody

var Present = preload("res://presents/Present.tscn")

var charging_start: int = 0;
var presents := []

func spawn_presents(count: int):
	for i in range(0, count):
		var present = Present.instance()
		get_parent().add_child(present)
		present.global_transform.origin = $PresentSpawnPoint.get_global_transform().origin
		present.translation.y += 0.25 * i
		presents.append(present)

func reset():
	for present in presents:
		present.queue_free()
	presents.clear()

func get_top_present():
	var presents = $PresentsArea.get_overlapping_bodies()
	var top_present = null
	var top_y := -1.0
	for present in presents:
		if present.translation.y > top_y:
			top_present = present
			top_y = top_present.translation.y
	return top_present

func point_at(point : Vector3):
	$PointerPivot.look_at(point, Vector3(0, 1, 0))

func get_pointer_vector() -> Vector3:
	var vector = Vector3(0, 0, 1)
	var global_rotation = $PointerPivot.global_transform.basis.get_euler()
	vector = vector.rotated(Vector3(0, 1, 0), global_rotation.y + PI)
	return vector.normalized()

func start_charging() -> void:
	charging_start = OS.get_ticks_msec()
	$AnimationPlayer.play("charging")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == 'charging':
		$AnimationPlayer.play("charged")

func stop_charging() -> int:
	$AnimationPlayer.play("reset")
	return OS.get_ticks_msec() - charging_start
