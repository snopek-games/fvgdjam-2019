extends Spatial

export (float) var camera_speed := 0.75

export (float) var sled_gravity := 1.0

export (float) var sled_default_speed := 45.0
export (float) var sled_min_speed := 20.0
export (float) var sled_max_speed := 200.0
export (float) var sled_speed_change_weight := 0.25
export (float) var sled_max_rotation := 135.0

export (float) var sled_rotation_speed := 3.0
export (float) var min_shoot_speed := 3.0
export (float) var max_shoot_speed := 7.0

var sled_vector := Vector3()
var sled_speed := 0.0
var started := false

var current_map = 0
var maps = [
	preload('res://environment/Maps/Map.tscn'),
	preload('res://environment/Maps/Map2.tscn'),
]
var show_next_map = false

func _ready() -> void:
	setup_map($Map)
	$UILayer.show_play_button()

func setup_map(map):
	map.connect('player_finished', self, '_on_Map_player_finished')

func show_map(scene):
	if has_node('Map'):
		$Map.queue_free()
		remove_child($Map)
	
	var map = scene.instance()
	map.name = 'Map'
	add_child(map)
	setup_map(map)

func start_game() -> void:
	started = true
	
	if show_next_map:
		current_map += 1
		if current_map > (maps.size() - 1):
			current_map = 0
		show_map(maps[current_map])
		show_next_map = false
	
	$Map.reset()
	
	sled_speed = sled_default_speed
	$Sled.translation = Vector3(2, 0, 2)
	$Camera.translation.x = 0
	
	$Sled.reset()
	$Sled.spawn_presents(4)

func end_game(message: String = "Finished!", button: String = "Try Again") -> void:
	started = false
	$UILayer.show_message(message)
	$UILayer.show_play_button(button)

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		var plane = Plane(Vector3(0, 1, 0), 0.0)
		var position = plane.intersects_ray($Camera.project_ray_origin(event.global_position), $Camera.project_ray_normal(event.global_position))
		$Sled.point_at(position)

func _process(delta: float) -> void:
	if started:
		$Camera.translation.x += (camera_speed * delta)
		
func _physics_process(delta: float) -> void:
	# Apply gravity
	var gravity_vector: Vector3 = ProjectSettings.get_setting("physics/3d/default_gravity_vector")
	var gravity_magnitude: int = ProjectSettings.get_setting("physics/3d/default_gravity")
	$Sled.move_and_slide(gravity_vector * gravity_magnitude, Vector3.UP)
	
	if not started:
		return
	
	# Figure out rotation from player input
	var rot_y = 0
	if Input.is_action_pressed("player_turn_left"):
		rot_y += 1
	if Input.is_action_pressed("player_turn_right"):
		rot_y -= 1
	if rot_y != 0:
		$Sled.rotate_y(rot_y * sled_rotation_speed * delta)
		if $Sled.rotation_degrees.y > 0:
			$Sled.rotation_degrees.y = clamp($Sled.rotation_degrees.y, sled_max_rotation, 180.0)
		else:
			$Sled.rotation_degrees.y = clamp($Sled.rotation_degrees.y, -180.0, -sled_max_rotation)
	
	# Adjust speed based on player input
	var backward := false
	if Input.is_action_pressed("player_forward"):
		sled_speed = lerp(sled_speed, sled_max_speed, sled_speed_change_weight)
	elif Input.is_action_pressed("player_backward"):
		sled_speed = lerp(sled_speed, sled_min_speed, sled_speed_change_weight)
		backward = true
	else:
		sled_speed = lerp(sled_speed, sled_default_speed, sled_speed_change_weight)
	
	# Adjust position based on angle and speed
	var vy = sled_vector.y
	sled_vector = Vector3(0, 0, 0)
	sled_vector += -$Sled.transform.basis.x
	sled_vector.y = vy
	sled_vector = sled_vector.normalized()
	$Sled.move_and_slide(sled_vector * sled_speed * delta, Vector3.UP)
	$Sled.translation.z = clamp($Sled.translation.z, 0.0, 4.0)
	
	# Make sure the player doesn't get behind the camera ...
	if $Sled.translation.x < $Camera.translation.x:
		$Sled.move_and_slide(Vector3($Camera.translation.x - $Sled.translation.x, 0, 0), Vector3.UP)
		# If it's still behind the camera, then we're dead. :-)
		if $Sled.translation.x < $Camera.translation.x - 1.0:
			end_game("Crushed")
	# ... and that they don't get ahead of the camera.
	elif $Sled.translation.x - $Camera.translation.x > 4.0:
		$Camera.translation.x = $Sled.translation.x - 4.0
	
	# Fire the present
	if Input.is_action_just_pressed("shoot"):
		$Sled.start_charging()
	if Input.is_action_just_released("shoot"):
		var charging_length = $Sled.stop_charging()
		var shoot_speed = clamp(max_shoot_speed * (float(charging_length) / 1000.0), min_shoot_speed, max_shoot_speed)
		var present : RigidBody = $Sled.get_top_present()
		if present:
			var present_vector = $Sled.get_pointer_vector()
			# Angle it up a little bit.
			present_vector = present_vector.rotated(Vector3(1, 0, 0), deg2rad(10))
			
			present.set_collision_mask_bit(3, false)
			present.apply_central_impulse(present_vector * shoot_speed)
			present.apply_torque_impulse(Vector3(randi() % 5 / 10.0, randi() % 5 / 10.0, randi() % 5 / 10.0))

func _on_UILayer_play_button_pressed() -> void:
	$UILayer.hide_all()
	start_game()

func _on_Map_player_finished() -> void:
	var score = $Map.get_score()
	if score[0] == score[1]:
		show_next_map = true
		if current_map < (maps.size() - 1):
			end_game("You got 'em all! Ready for the next level?", "Play Next Level")
		else:
			end_game("You beat the game! Congrats!", "Try From The Start")
	else:
		var delivery_rate = int((float(score[0]) / float(score[1])) * 100)
		var message = "Delivery Rate: " + str(delivery_rate) + "%"
		if delivery_rate < 50:
			message += " :-("
		elif delivery_rate < 75:
			message += " - not bad!"
		else:
			message += " - almost!"
		end_game(message)
