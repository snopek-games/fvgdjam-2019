extends CanvasLayer

signal play_button_pressed

func show_play_button(text: String = 'Play') -> void:
	$PlayButton.text = text
	$PlayButton.show()

func hide_play_button() -> void:
	$PlayButton.hide()

func show_message(text: String) -> void:
	$Label.text = text
	$Label.show()

func hide_message() -> void:
	$Label.hide()

func hide_all() -> void:
	hide_play_button()
	hide_message()

func _on_PlayButton_pressed() -> void:
	emit_signal("play_button_pressed")
