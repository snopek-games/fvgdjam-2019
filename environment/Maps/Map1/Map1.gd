extends Spatial

signal player_finished

func reset() -> void:
	get_tree().call_group("present_detector", "reset")
	get_tree().call_group("powerups", "reset")

func get_score():
	var present_detectors = get_tree().get_nodes_in_group("present_detector")
	var total := 0
	var count := 0
	for present_detector in present_detectors:
		total += 1
		if present_detector.is_present_detected():
			count += 1
	return [count, total]

func _on_FinishArea_body_entered(body: Node) -> void:
	emit_signal("player_finished")
