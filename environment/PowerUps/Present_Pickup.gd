extends Area


func _process(delta):
	rotate_y(deg2rad(60 * delta))

func reset():
	visible = true
	$CollisionShape.disabled = false

func _on_Present_Pickup_body_entered(body):
	if body.has_method('spawn_presents'):
		body.spawn_presents(1)
		visible = false
		$CollisionShape.disabled = true
