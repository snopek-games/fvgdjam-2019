extends Area

onready var inactive_material = $BeamMesh.get_surface_material(0)
var active_material = preload("res://environment/PresentDetectorActive.tres")

var present_count := 0

func is_present_detected() -> bool:
	return present_count > 0

func reset() -> void:
	$BeamMesh.set_surface_material(0, inactive_material)
	present_count = 0

func _on_PresentDetector_body_entered(body: Node) -> void:
	$BeamMesh.set_surface_material(0, active_material)
	present_count += 1
	$AudioPlayer.play()
	

func _on_PresentDetector_body_exited(body: Node) -> void:
	present_count -= 1
	if present_count <= 0:
		reset()
